extends Control


signal btn_back_wallpapers_pressed


const PATH_TO_WALLPAPERS: String = "res://source/assets/image/wallpaper/wallpaper_"

var wallpaper_opened: bool = false


# BUILTINS - - - - - - - - -


func _ready() -> void:
	show_wallpapers(false)
	($Wallpaper as Control).set("PATH_TO_WALLPAPERS", PATH_TO_WALLPAPERS)
	show_wallpaper(false)


# METHODS - - - - - - - - -


func show_wallpapers(state: bool) -> void:
	var _t: bool
	if state:
		$".".show()
		_t = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 1.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		_t = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 0.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(0.3), "timeout")
		$".".hide()


func show_wallpaper(state: bool) -> void:
	var _t: bool
	if state:
		wallpaper_opened = true
		($Wallpaper as Control).call("set_image")
		($Wallpaper as Control).show()
		_t = ($Tween as Tween).interpolate_property($Wallpaper as Control, "modulate:a", null, 1.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		wallpaper_opened = false
		_t = ($Tween as Tween).interpolate_property($Wallpaper as Control, "modulate:a", null, 0.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(0.3), "timeout")
		($Wallpaper as Control).hide()


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	if wallpaper_opened:
		show_wallpaper(false)
	else:
		emit_signal("btn_back_wallpapers_pressed")
		show_wallpapers(false)


func _on_Btn1_pressed() -> void:
	($Wallpaper as Control).set("IMAGE", 1)
	show_wallpaper(true)


func _on_Btn2_pressed() -> void:
	($Wallpaper as Control).set("IMAGE", 2)
	show_wallpaper(true)


func _on_Btn3_pressed() -> void:
	($Wallpaper as Control).set("IMAGE", 3)
	show_wallpaper(true)
