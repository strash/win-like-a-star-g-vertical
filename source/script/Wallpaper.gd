extends Control


var PATH_TO_WALLPAPERS: String = ""
var IMAGE: int = 1


# BUILTINS - - - - - - - - -


# METHODS - - - - - - - - -


func set_image() -> void:
	($Image as TextureRect).texture = load(PATH_TO_WALLPAPERS + str(IMAGE) + ".png")


# SIGNALS - - - - - - - - -


func _on_BtnDownload_pressed() -> void:
	var js = """
	const a = document.createElement('a');
	a.href = "wallpaper/wallpape_%s.png";
	a.setAttribute("download", "wallpape_%s.png")
	a.style.display = 'none';
	document.body.appendChild(a);
	a.click();
	a.remove();
	""" % [IMAGE, IMAGE]
	JavaScript.eval(js)
