extends Control


signal btn_slot_pressed
signal btn_wallpaper_pressed


# BUILTINS - - - - - - - - -


# METHODS - - - - - - - - -


func show_menu(state: bool) -> void:
	var _t: bool
	if state:
		$".".show()
		_t = ($Tween as Tween).interpolate_property(self, "modulate:a", 0.0, 1.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		_t = ($Tween as Tween).interpolate_property(self, "modulate:a", 1.0, 0.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(0.3), "timeout")
		$".".hide()



# SIGNALS - - - - - - - - -


func _on_BtnSlot_pressed() -> void:
	emit_signal("btn_slot_pressed")


func _on_BtnWallpaper_pressed() -> void:
	emit_signal("btn_wallpaper_pressed")