extends Control


signal level1_pressed
signal level2_pressed
signal btn_back_levels_pressed


# SIGNALS - - - - - - - - -


func _on_BtnLvl1_pressed() -> void:
	emit_signal("level1_pressed")


func _on_BtnLvl2_pressed() -> void:
	emit_signal("level2_pressed")


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_back_levels_pressed")
